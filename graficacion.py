# -*- coding: utf-8 -*-
"""
Created on Thu Jun  4 15:29:17 2020

@author: Mateo
"""

import librosa
import librosa.display
import matplotlib.pyplot as plt
import numpy as np
import scipy.signal as signal;
import scipy.io as sio
import glob
import os
import pandas as pd

get_ipython().run_line_magic('matplotlib','qt')

#Importo la rutina de funciones
import Funciones as funciones

#Importo la rutina de filtro frecuencia
import filtrado_frecuencia as filtro


#Se importan todos los archivos de la ruta donde está la carpeta
ruta_archivos = '/Users/Mateo/Desktop/Señales/Proyecto 3/respiratory-sound-database/Respiratory_Sound_Database/Respiratory_Sound_Database/audio_and_txt_files'



archivostexto = glob.glob(ruta_archivos + '/*.txt',recursive = True)
archivosaudio = glob.glob(ruta_archivos + '/*.wav',recursive = True)

total_crepitancias= []
total_sibilancias=[]
total_sanos=[]

#Se clasifican los ciclos entre 
i=900
    
tinicio, tfinal, crepitancias, sibilancias, sr, sfiltrada = funciones.FiltroCompleto(archivosaudio[i], archivostexto[i])
#Carga y filtrado en frecuencia y wavelet
total_sanos, total_sibilancias, total_crepitancias = funciones.clasificacion(total_sanos, total_crepitancias, total_sibilancias,archivosaudio[i], archivostexto[i], sfiltrada, tinicio, tfinal, crepitancias, sibilancias)