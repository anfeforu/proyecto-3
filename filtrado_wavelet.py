# -*- coding: utf-8 -*-
"""
Created on Fri May 29 10:32:34 2020

@author: anfef
"""

import librosa
import librosa.display
import pywt
import numpy as np
import matplotlib.pyplot as plt;
import scipy.io as sio;
from scipy.io.wavfile import write

def wthresh(coeff,thr):
    y   = list();
    s = wnoisest(coeff);
    for i in range(0,len(coeff)):
        y.append(np.multiply(coeff[i],np.abs(coeff[i])>(thr*s[i])));
    return y;
    
def thselect(signal):
    Num_samples = 0;
    for i in range(0,len(signal)):
        Num_samples = Num_samples + signal[i].shape[0];
    
    thr = np.sqrt(2*(np.log(Num_samples)))
    return thr

def wnoisest(coeff):
    stdc = np.zeros((len(coeff),1));
    for i in range(1,len(coeff)):
        stdc[i] = (np.median(np.absolute(coeff[i])))/0.6745;
    return stdc;

def wavelet(signal):
    
#    signal= R'Respiratory_Sound_Database\audio_and_txt_files\101_1b1_Al_sc_Meditron.wav'
#    data, sr= librosa.load(signal)
    data = signal
    
    LL = int(np.floor(np.log2(data.shape[0])));
    
    coeff = pywt.wavedec( data, 'db6', level=LL );
    
    thr = thselect(coeff);
    coeff_t = wthresh(coeff,thr);
    
    x_rec = pywt.waverec( coeff_t, 'db6');
    
    x_rec = x_rec[0:data.shape[0]];
    
#    plt.plot(data[0:15000],label='Original')
#    plt.plot(x_rec[0:15000],label='Umbralizada por Wavelet')
    
    x_filt = np.squeeze(data - x_rec);
#    plt.plot(x_filt[0:15000],label='Original - Umbralizada')
#    plt.legend()

#    write('test6db.wav', sr, x_filt)    
    return x_filt
