# -*- coding: utf-8 -*-
"""
Created on Thu May 21 13:23:07 2020

@author: Mateo
"""

import librosa
import librosa.display
import matplotlib.pyplot as plt
import numpy as np
import scipy.signal as signal;
#Importa Wavelet
#Importo la rutina de filtro frecuencia
import filtrado_wavelet
import filtrado_frecuencia as filtro

get_ipython().run_line_magic('matplotlib','qt')

def CargarSenal (archivoaudio):
    
    original, sr= librosa.load(archivoaudio)
    orden, bandpass= filtro.filter_design(sr, locutoff = 100, hicutoff = 2000, revfilt = 0)
    audiofiltrado= signal.filtfilt(bandpass, 1, original)
    
    
    return original,audiofiltrado,sr

def FiltroCompleto (archivoaudio, archivotexto):
    info = np.loadtxt(archivotexto)
    
    
    original,sfiltrada1,sr = CargarSenal(archivoaudio)
    sfiltradacompleta=filtrado_wavelet.wavelet(sfiltrada1)
    
    tinicio= info[:,0]*sr
    tfinal= info[:,1]*sr
    crepitancias= info[:,2]
    sibilancias= info[:,3]
    
#    filtrofrec=np.asfortranarray(sfiltrada1)
#    filtro= np.asfortranarray(sfiltradacompleta)
    
    
#        Graficas de ciclos
    
    librosa.display.waveplot((original[int(np.round(tinicio[1])):int(np.round(tfinal[1]))]), sr=sr);
    plt.figure()
    librosa.display.waveplot((filtrofrec[int(np.round(tinicio[1])):int(np.round(tfinal[1]))]), sr=sr);
    plt.figure()
    librosa.display.waveplot((filtro[int(np.round(tinicio[1])):int(np.round(tfinal[1]))]), sr=sr);
    plt.figure()
    
    return tinicio, tfinal, crepitancias, sibilancias, sr, sfiltradacompleta
    
def clasificacion (total_sanos, total_crepitancias, total_sibilancias, total_ambos, archivoaudio, archivotexto, sfiltrada, tinicio, tfinal, crepitancias, sibilancias):
    
    
    
    for i in range(len(crepitancias)):
        
        
        
        if crepitancias[i] == 1 and sibilancias[i] == 0:
            
            total_crepitancias.append(sfiltrada[int(np.round(tinicio[i])):int(np.round(tfinal[i]))])
            
        elif crepitancias[i] == 0 and sibilancias[i] == 1:
            
            total_sibilancias.append(sfiltrada[int(np.round(tinicio[i])):int(np.round(tfinal[i]))])
            
        elif crepitancias[i] == 0 and sibilancias[i] == 0:
            
            total_sanos.append(sfiltrada[int(np.round(tinicio[i])):int(np.round(tfinal[i]))])
        
        elif crepitancias[i] == 1 and sibilancias[i] == 1:
            
            total_ambos.append(sfiltrada[int(np.round(tinicio[i])):int(np.round(tfinal[i]))])
            
    return total_sanos, total_sibilancias, total_crepitancias, total_ambos

def Promedio_Espectro (ciclo, sr):
    f, Pxx = signal.welch(ciclo)
    promedio= np.mean(Pxx)
    return promedio

def Rango(ciclo):
    rango= abs(np.amax(ciclo)-np.amin(ciclo))
    return rango

def Varianza(ciclo):
    varianza=np.var(ciclo)
    return varianza

def SMA(ciclo):
    prom_movil=0
    for i in range(len(ciclo)-1):
        prom_movil=prom_movil+abs((ciclo[i]-ciclo[i+1])) 
    return prom_movil