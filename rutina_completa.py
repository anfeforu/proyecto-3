# -*- coding: utf-8 -*-
"""
Created on Mon May 25 13:31:56 2020

@author: Mateo
"""

import librosa
import librosa.display
import matplotlib.pyplot as plt
import numpy as np
import scipy.signal as signal;
import scipy.io as sio
import scipy.stats as stats
import glob
import os
import pandas as pd

get_ipython().run_line_magic('matplotlib','qt')

#Importo la rutina de funciones
import Funciones as funciones

#Importo la rutina de filtro frecuencia
import filtrado_frecuencia as filtro


#Se importan todos los archivos de la ruta donde está la carpeta
ruta_archivos = R'Respiratory_Sound_Database\audio_and_txt_files'


archivostexto = glob.glob(ruta_archivos + '/*.txt',recursive = True)
archivosaudio = glob.glob(ruta_archivos + '/*.wav',recursive = True)

total_crepitancias= []
total_sibilancias=[]
total_sanos=[]
total_ambos=[]

#Se clasifican los ciclos entre 
for i in range(len(archivosaudio)):
    
    tinicio, tfinal, crepitancias, sibilancias, sr, sfiltrada = funciones.FiltroCompleto(archivosaudio[i], archivostexto[i])
    #Carga y filtrado en frecuencia y wavelet
    total_sanos, total_sibilancias, total_crepitancias, total_ambos = funciones.clasificacion(total_sanos, total_crepitancias, total_sibilancias, total_ambos, archivosaudio[i], archivostexto[i], sfiltrada, tinicio, tfinal, crepitancias, sibilancias)


varianzasSanos= []
varianzasCrepitancias= []
varianzasSibilancias=[]
varianzasAmbos=[]

rangosSanos= []
rangosCrepitancias= []
rangosSibilancias=[]
rangosAmbos=[]

promEspectroSanos= []
promEspectroCrepitancias= []
promEspectroSibilancias=[]
promEspectroAmbos=[]

promMovilSanos= []
promMovilCrepitancias= []
promMovilSibilancias=[]
promMovilAmbos=[]

for i in range(len(total_sanos)):
    rango=funciones.Rango(total_sanos[i])
    rangosSanos.append(rango)
    
    promesp= funciones.Promedio_Espectro(total_sanos[i],sr)
    promEspectroSanos.append(promesp)
    
    varianza=funciones.Varianza(total_sanos[i])
    varianzasSanos.append(varianza)
    
    promMovil=funciones.SMA(total_sanos[i])
    promMovilSanos.append(promMovil)
    
for i in range(len(total_crepitancias)):
    rango=funciones.Rango(total_crepitancias[i])
    rangosCrepitancias.append(rango)
    
    promesp= funciones.Promedio_Espectro(total_crepitancias[i],sr)
    promEspectroCrepitancias.append(promesp)

    varianza=funciones.Varianza(total_crepitancias[i])
    varianzasCrepitancias.append(varianza)
    
    promMovil=funciones.SMA(total_crepitancias[i])
    promMovilCrepitancias.append(promMovil)
    
for i in range(len(total_sibilancias)):
    rango=funciones.Rango(total_sibilancias[i])
    rangosSibilancias.append(rango)
    
    promesp= funciones.Promedio_Espectro(total_sibilancias[i],sr)
    promEspectroSibilancias.append(promesp)
    
    varianza=funciones.Varianza(total_sibilancias[i])
    varianzasSibilancias.append(varianza)

    promMovil=funciones.SMA(total_sibilancias[i])
    promMovilSibilancias.append(promMovil)

for i in range(len(total_ambos)):
    rango=funciones.Rango(total_ambos[i])
    rangosAmbos.append(rango)
    
    promesp= funciones.Promedio_Espectro(total_ambos[i],sr)
    promEspectroAmbos.append(promesp)
    
    varianza=funciones.Varianza(total_ambos[i])
    varianzasAmbos.append(varianza)

    promMovil=funciones.SMA(total_ambos[i])
    promMovilAmbos.append(promMovil)
    
#Ahora se contruyen los dataframe

#DataFrame de SANOS  
DF_Sanos= pd.DataFrame({'Rangos': rangosSanos, 'Prom.Espectro' : promEspectroSanos, 'Varianzas': varianzasSanos, 'Prom.Movil': promMovilSanos}, columns=['Rangos', 'Prom.Espectro', 'Varianzas', 'Prom.Movil'])
print(DF_Sanos)
DF_Sanos.to_csv('DataFrame_Sanos.csv')

DF_Sanos.describe()

#DataFrame de Crepitancias  
DF_Crepitancias= pd.DataFrame({'Rangos': rangosCrepitancias, 'Prom.Espectro' : promEspectroCrepitancias, 'Varianzas': varianzasCrepitancias, 'Prom.Movil': promMovilCrepitancias}, columns=['Rangos', 'Prom.Espectro', 'Varianzas', 'Prom.Movil'])
print(DF_Crepitancias)
DF_Crepitancias.to_csv('DataFrame_Crepitancias.csv')

DF_Crepitancias.describe()

#DataFrame de Sibilancias  
DF_Sibilancias= pd.DataFrame({'Rangos': rangosSibilancias, 'Prom.Espectro' : promEspectroSibilancias, 'Varianzas': varianzasSibilancias, 'Prom.Movil': promMovilSibilancias}, columns=['Rangos', 'Prom.Espectro', 'Varianzas', 'Prom.Movil'])
print(DF_Sibilancias)
DF_Sibilancias.to_csv('DataFrame_Sibilancias.csv')

DF_Sibilancias.describe()

#DataFrame de Sibilancias y Crepitancias
DF_Ambos= pd.DataFrame({'Rangos': rangosAmbos, 'Prom.Espectro' : promEspectroAmbos, 'Varianzas': varianzasAmbos, 'Prom.Movil': promMovilAmbos}, columns=['Rangos', 'Prom.Espectro', 'Varianzas', 'Prom.Movil'])
print(DF_Ambos)
DF_Ambos.to_csv('DataFrame_Ambos.csv')

indices=['Rangos','Prom.Espectro','Varianzas','Prom.Movil']

#Se crean los Histogramas

#Histograma de sujetos Sanos
plt.figure()
DatosSanos=pd.read_csv('DataFrame_Sanos.csv')
DF_Sanos=pd.DataFrame(DatosSanos)
for i in range(len(indices)):
    count, bin_edges = np.histogram(DF_Sanos[indices[i]])
    DF_Sanos[indices[i]].plot(kind='hist', xticks=bin_edges)
    plt.title('Sanos')
    plt.xlabel('Valor de '+ indices[i])
    plt.ylabel('Cantidad')
    plt.grid()
    if i < len(indices)-1:
        plt.figure()

#Histograma de pacientes con Crepitancias
DatosCrepi=pd.read_csv('DataFrame_Crepitancias.csv')
DF_Crepitancias=pd.DataFrame(DatosCrepi)
plt.figure()
for i in range(len(indices)):
    count, bin_edges = np.histogram(DF_Crepitancias[indices[i]])
    DF_Crepitancias[indices[i]].plot(kind='hist', xticks=bin_edges)
    plt.title('Crepitancias')
    plt.xlabel('Valor de '+ indices[i])
    plt.ylabel('Cantidad')
    plt.grid()
    if i < len(indices)-1:
        plt.figure()

#Histograma de sujetos Sibilancias
DatosSibi=pd.read_csv('DataFrame_Sibilancias.csv')
DF_Sibilancias=pd.DataFrame(DatosSibi)
plt.figure()
for i in range(len(indices)):
    count, bin_edges = np.histogram(DF_Sibilancias[indices[i]])
    DF_Sibilancias[indices[i]].plot(kind='hist', xticks=bin_edges)
    plt.title('Sibilancias')
    plt.xlabel('Valor de '+ indices[i])
    plt.ylabel('Cantidad')
    plt.grid()
    if i < len(indices)-1:
        plt.figure()

#Histograma de sujetos Crepitancias y Sibilancias
DatosAmbos=pd.read_csv('DataFrame_Ambos.csv')
DF_Ambos=pd.DataFrame(DatosAmbos)
plt.figure()
for i in range(len(indices)):
    count, bin_edges = np.histogram(DF_Ambos[indices[i]])
    DF_Ambos[indices[i]].plot(kind='hist', xticks=bin_edges)
    plt.title('Sibilancias y Crepitancias')
    plt.xlabel('Valor de '+ indices[i])
    plt.ylabel('Cantidad')
    plt.grid()
    if i < len(indices)-1:
        plt.figure()

#Creación de arreglos para realizar el análisis estadístico

#Creación de arreglos Sanos    
Rangos_Sanos=np.array(DF_Sanos['Rangos'])
Espectro_Sanos=np.array(DF_Sanos['Prom.Espectro'])
Varianzas_Sanos=np.array(DF_Sanos['Varianzas'])
Movil_Sanos=np.array(DF_Sanos['Prom.Movil'])

#Creación de arreglos con Crepitancias
Rangos_Crepitancias=np.array(DF_Crepitancias['Rangos'])
Espectro_Crepitancias=np.array(DF_Crepitancias['Prom.Espectro'])
Varianzas_Crepitancias=np.array(DF_Crepitancias['Varianzas'])
Movil_Crepitancias=np.array(DF_Crepitancias['Prom.Movil'])

#Creación de arreglos con Sibilancias
Rangos_Sibilancias=np.array(DF_Sibilancias['Rangos'])
Espectro_Sibilancias=np.array(DF_Sibilancias['Prom.Espectro'])
Varianzas_Sibilancias=np.array(DF_Sibilancias['Varianzas'])
Movil_Sibilancias=np.array(DF_Sibilancias['Prom.Movil'])

#Análisis estadístico por U de Mann-Whitney para pruebas no pareadas
stRSaC, pRSaC = stats.mannwhitneyu(Rangos_Sanos, Rangos_Crepitancias)
stRSaSi, pRSaSi = stats.mannwhitneyu(Rangos_Sanos, Rangos_Sibilancias)
stRSiC, pRSiC = stats.mannwhitneyu(Rangos_Sibilancias, Rangos_Crepitancias)

stESaC, pESaC = stats.mannwhitneyu(Espectro_Sanos, Espectro_Crepitancias)
stESaSi, pESaSi = stats.mannwhitneyu(Espectro_Sanos, Espectro_Sibilancias)
stESiC, pESiC = stats.mannwhitneyu(Espectro_Sibilancias, Espectro_Crepitancias)

stVSaC, pVSaC = stats.mannwhitneyu(Varianzas_Sanos, Varianzas_Crepitancias)
stVSaSi, pVSaSi = stats.mannwhitneyu(Varianzas_Sanos, Varianzas_Sibilancias)
stVSiC, pVSiC = stats.mannwhitneyu(Varianzas_Sibilancias, Varianzas_Crepitancias)

stMSaC, pMSaC = stats.mannwhitneyu(Movil_Sanos, Movil_Crepitancias)
stMSaSi, pMSaSi = stats.mannwhitneyu(Movil_Sanos, Movil_Sibilancias)
stMSiC, pMSiC = stats.mannwhitneyu(Movil_Sibilancias, Movil_Crepitancias)

#Creación de Dataframes para organizar la información de la prueba estadística
comp = ['Sanos Vs Crepitancias', 'Sanos Vs Sibilancias', 'Sibilancias Vs Crepitancias']

#Dataframe de los valores estadísticos
stR = [stRSaC, stRSaSi, stRSiC]
stE = [stESaC, stESaSi, stESiC]
stV = [stVSaC, stVSaSi, stVSiC]
stM = [stMSaC, stMSaSi, stMSiC]

DF_st= pd.DataFrame({'Comparación': comp,'Estadístico Rangos': stR, 'Estadístico Prom.Espectro' : stE, 'Estadístico Varianzas': stV, 'Estadístico Prom.Movil': stM}, columns=['Comparación','Estadístico Rangos', 'Estadístico Prom.Espectro', 'Estadístico Varianzas', 'Estadístico Prom.Movil'])
print(DF_st)
DF_st.to_csv('Dataframe_Estadistico.csv')

#Dataframe de los valres p
valpR = [pRSaC, pRSaSi, pRSiC]
valpE = [pESaC, pESaSi, pESiC]
valpV = [pVSaC, pVSaSi, pVSiC]
valpM = [pMSaC, pMSaSi, pMSiC]

DF_valp= pd.DataFrame({'Comparación': comp,'Val p Rangos': valpR, 'Val p Prom.Espectro' : valpE, 'Val p Varianzas': valpV, 'Val p Prom.Movil': valpM}, columns=['Comparación','Val p Rangos', 'Val p Prom.Espectro', 'Val p Varianzas', 'Val p Prom.Movil'])
print(DF_valp)
DF_valp.to_csv('Datafrema_valorp.csv')

DF_Ambos.describe()
DF_Crepitancias.describe()
DF_Sanos.describe()
DF_Sibilancias.describe()
